from django.shortcuts import render, redirect
from lab_1.views import mhs_name, birth_date
from .models import Diary
from datetime import datetime
import pytz
import json

'''
    #Create a list of biodata that you wanna show on webpage:
    #[{'subject' : 'Name', 'value' : 'Igor'},{{'subject' : 'Birth Date', 'value' : '11 August 1970'},{{'subject' : 'Sex', 'value' : 'Male'}
    #TODO Implement
bio_dict = [{'subject' : 'Name', 'value' : mhs_name},\
{'subject' : 'Birth Date', 'value' : birth_date.strftime('%d %B %Y')},\
{'subject' : 'Sex', 'value' : 'Male'}]
'''

def index(request):
	diary_dict = Diary.objects.all().values()
	return render(request, 'to_do_list.html', {'diary_dict' : convert_queryset_into_json(diary_dict)})
	
def add_activity(request):
	if request.method == 'POST':
		date = datetime.strptime(request.POST['date'],'%Y-%m-%dT%H:%M')
		Diary.objects.create(date=date.replace(tzinfo=pytz.UTC),activity=request.POST['activity'])
		return redirect('/lab-3/')
		
def convert_queryset_into_json(queryset):
	ret_val = []
	for data in queryset:
		ret_val.append(data)
	return ret_val