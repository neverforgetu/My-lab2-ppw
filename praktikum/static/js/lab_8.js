var currentName;
// FB initiation function
window.fbAsyncInit = () => {
    FB.init({
        appId      : '1520593271581141',
        cookie     : true,
        xfbml      : true,
        version    : 'v2.11'
    });
};

// Call init facebook. default dari facebook
(function(d, s, id){
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) {return;}
    js = d.createElement(s); js.id = id;
    js.src = 'https://connect.facebook.net/en_US/sdk.js';
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));


const renderProfile = () => {
    getUserData(user => {
        currentName = user.name;
        $('.profile-name').text(user.name);
        $('.profile-about').text(user.about);
        $('.profile-email').text(user.email);
        if (user.gender === 'male') $('.gender-female').hide();
        else $('.gender-male').hide();
        $('.profile-cover').attr('src', user.cover.source);
        $('.profile-picture').attr('src', user.picture.data.url);
        $('#lab8').show('slow');
    });
};

const renderFeed = () => {
    getUserFeed((feed, name) => {
        feed.data.map(value => {
            var heading, content;
            if (value.message && !value.story) {
                heading = '<div class="card-header"><h4 class="card-title">'+ name +'</h4></div>';
                content = '<p class="card-text">' + value.message + '</p>';
            }else if (value.message && value.story) {
                heading = '<div class="card-header"><h4 class="card-title">'+ value.story +'</h4></div>';
                content = '<p class="card-text">' + value.message + '</p>';
            }else {
                heading = '';
                content = '<p class="card-text">' + value.story + '</p>';
            }
            const footer = '<div class="card-footer text-muted">' + (new Date(value.created_time)).toString() + '</div>';
            $('#feeds').append(
                '<div class="row mt-5">' + 
                '<div class="card w-100">' +
                heading +
                '<div class="card-body">' +
                content +
                '</div>' +
                footer +
                '</div>' +
                '</div>'
            );
        });
    });
};

const postFeed = (message, callback) => {
    FB.api('/me/feed', 'POST', {
        message: message,
        application: 1928514094137748
    }, response => {
        console.log('status posted:');
        console.log(response);
        callback();
    });
};

const renderStatusForm = () => {
    const heading = '<div class="card-header"><h4 class="card-title" id="update-title">Update Status</h4></div>';
    const content = '<div class="form" id="update-form"><div class="form-group"><textarea maxlength="64" id="update-textarea" type="text" class="form-control"></textarea></div><button type="submit" id="update-status" class="btn btn-primary">Post</button></div>' + 
    '<p class="card-text" id="update-message"></p>';
    const footer = '<div class="card-footer text-muted" id="update-date">Now</div>';
    const form = $(
        '<div class="row mt-5">' + 
        '<div class="card w-100">' +
        heading +
        '<div class="card-body">' +
        content +
        '</div>' +
        footer +
        '</div>' +
        '</div>'
    );
    form.hide();
    const temp = $('#feeds').html();
    $('#feeds').html('');
    $('#feeds').append(form);    
    $('#feeds').append(temp);
    form.show('normal');
    autosize(document.querySelector('textarea'));  
    $('#update-status').click(() => {
        const message = $('#update-textarea').val();
        $('#update-status').hide('normal');
        $('#update-title').text('posting new status...');
        postFeed(message, () => {
            $('#update-message').text(message);
            $('#update-form').hide('1000', () => {
                $('#update-title').text(currentName);
                $('#update-form').remove();
                $('#update-date').text(Date().toString());
                $('#update-date').attr('id', 'xxx'+Math.random().toString());
                $('#update-message').attr('id', 'xxx'+Math.random().toString());
                $('#update-title').attr('id', 'xxx'+Math.random().toString());
                renderStatusForm();
            });
        });
    });
};

// Fungsi Render, mengecek apakah user sudah login, jika sudah, render data
const render = () => {
    $('#loading-1').show();
    console.log('render!');
    FB.getLoginStatus(response => {
        if (response.status == 'connected'){
            $('#not-logged-in').hide();
            renderProfile();
            $('#loading-1').hide();
            renderFeed();
            renderStatusForm();
        } else {
            // Tampilan ketika belum login
            $('#lab8').hide('fast');
            $('#feeds').html('');
            $('#not-logged-in').show();
            $('#loading-1').hide('slow');
        }
    });
    
};

setTimeout(render, 3000);

const getUserData = callback => {
    FB.getLoginStatus(response => {
        if (response.status === 'connected') {
            FB.api('/me?fields=id,name,cover,picture.type(large),about,email,gender', 'GET', response => {
                console.log(response);
                callback(response);
            });
        }
    });

};

const getUserFeed = (callback) => {
    FB.api('/me?fields=feed,name', 'GET', response => {
        callback(response.feed, response.name);
    });
};